# https://adventofcode.com/2017/day/25

from collections import namedtuple
from enum import Enum
from unittest import TestCase


State = namedtuple("State", ["write", "move", "next"])


class TuringMachine:
    def __init__(self, states):
        self.states = states

        self.tape = [0]
        self.cursor = 0
        self.state = "A"

    def read_value(self):
        return self.tape[self.cursor]

    def write_value(self, value):
        self.tape[self.cursor] = value

    def move(self, direction):
        self.cursor += direction
        if self.cursor == len(self.tape):
            self.tape.append(0)
        elif self.cursor == -1:
            self.tape.insert(0, 0)
            self.cursor = 0

        assert 0 <= self.cursor < len(self.tape)

    def step(self):
        state = self.states[(self.state, self.read_value())]
        self.write_value(state.write)
        self.move(state.move)
        self.state = state.next

    def run(self, steps):
        for _ in range(steps):
            self.step()

    def checksum(self):
        return self.tape.count(1)


class TestSolution(TestCase):
    STATES = {
        ("A", 0): State(1, 1, "B"),
        ("A", 1): State(0, -1, "B"),
        ("B", 0): State(1, -1, "A"),
        ("B", 1): State(1, 1, "A"),
    }

    def test_run(self):
        machine = TuringMachine(self.STATES)
        machine.run(steps=6)
        self.assertEqual(machine.tape, [1, 1, 0, 1])
        self.assertEqual(machine.checksum(), 3)


def main():
    input_states = {
        ("A", 0): State(1, 1, "B"),
        ("A", 1): State(0, -1, "D"),
        ("B", 0): State(1, 1, "C"),
        ("B", 1): State(0, 1, "F"),
        ("C", 0): State(1, -1, "C"),
        ("C", 1): State(1, -1, "A"),
        ("D", 0): State(0, -1, "E"),
        ("D", 1): State(1, 1, "A"),
        ("E", 0): State(1, -1, "A"),
        ("E", 1): State(0, 1, "B"),
        ("F", 0): State(0, 1, "C"),
        ("F", 1): State(0, 1, "E"),
    }
    machine = TuringMachine(input_states)
    machine.run(steps=12317297)
    print(machine.checksum())


if __name__ == "__main__":
    main()

# https://adventofcode.com/2017/day/11

from unittest import TestCase

import numpy as np

# Fields in a hexagon grid are represented in cube coordinates:
# https://www.redblobgames.com/grids/hexagons/#coordinates-cube


def move(field, direction):
    delta = {
        "n": (0, 1, -1),
        "ne": (1, 0, -1),
        "se": (1, -1, 0),
        "s": (0, -1, 1),
        "sw": (-1, 0, 1),
        "nw": (-1, 1, 0),
    }[direction]
    return field + delta


def distance(field1, field2):
    diff = abs(field1 - field2)
    return sum(diff) // 2


def path_distance(path):
    start = np.array((0, 0, 0))
    field = start
    for direction in path:
        field = move(field, direction)
    return distance(start, field)


def max_distance(path):
    start = np.array((0, 0, 0))
    field = start
    max_dist = 0
    for direction in path:
        field = move(field, direction)
        dist = distance(start, field)
        if dist > max_dist:
            max_dist = dist
    return max_dist



class TestSolution(TestCase):
    def test_path_distance(self):
        self.assertEqual(path_distance(["ne", "ne", "ne"]), 3)
        self.assertEqual(path_distance(["ne", "ne", "sw", "sw"]), 0)
        self.assertEqual(path_distance(["ne", "ne", "s", "s"]), 2)
        self.assertEqual(path_distance(["se", "sw", "se", "sw", "sw"]), 3)


def main():
    with open("input.txt", "rt") as fobj:
        inp = fobj.read().strip()
    path = inp.split(",")
    print("part 1:", path_distance(path))
    print("part 2:", max_distance(path))


if __name__ == "__main__":
    main()

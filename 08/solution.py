# https://adventofcode.com/2017/day/8

from collections import defaultdict
from unittest import TestCase


class Registers:
    def __init__(self):
        self.regs = defaultdict(int)
        self.all_time_max = 0

    def get(self, reg):
        return self.regs[reg]

    def inc(self, reg, value):
        self.regs[reg] += value
        self.all_time_max = max(self.all_time_max, self.regs[reg])

    def dec(self, reg, value):
        self.regs[reg] -= value
        self.all_time_max = max(self.all_time_max, self.regs[reg])

    def current_max(self):
        return max(self.regs.values())


def exec_program(program):
    regs = Registers()
    for inst in program.splitlines():
        exec_inst(inst, regs)
    return regs


def exec_inst(inst, regs):
    cmd, cond = inst.split(" if ")

    cond_reg, cmp = cond.split(" ", maxsplit=1)
    cond = "regs.get('{}') {}".format(cond_reg, cmp)
    if not eval(cond):
        return

    reg, op, num = cmd.split()
    num = int(num)
    if op == "inc":
        regs.inc(reg, num)
    else:
        assert op == "dec"
        regs.dec(reg, num)


class TestSolution(TestCase):
    INPUT = ("b inc 5 if a > 1\n"
             "a inc 1 if b < 5\n"
             "c dec -10 if a >= 1\n"
             "c inc -20 if c == 10")

    def test_final_max(self):
        regs = exec_program(self.INPUT)
        self.assertEqual(regs.current_max(), 1)

    def test_all_time_max(self):
        regs = exec_program(self.INPUT)
        self.assertEqual(regs.all_time_max, 10)


def main():
    with open("input.txt", "rt") as fobj:
        inp = fobj.read()
    regs = exec_program(inp)
    print("part 1:", max(regs.regs.values()))
    print("part 2:", regs.all_time_max)


if __name__ == "__main__":
    main()
